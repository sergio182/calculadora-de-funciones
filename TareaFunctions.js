//Calculadora con funciones

//Funcion suma
function sumar(valor1, valor2){
   resultado = valor1 + valor2;
   return "El resultado de la suma es " + resultado;
}
console.log(sumar(10,15));

//Funcion Resta
function restar(valor1, valor2){
    resultado = valor1 - valor2;
    return "El resultado de la resta es " + resultado;
}
console.log(restar(10,5));

//Funcion Multiplicacion
function multiplicacion(valor1, valor2){
    resultado = valor1 * valor2;
    return "El resultado de la multiplicacion es " + resultado;
}
console.log(multiplicacion(5,5))

//Funcion Divison
function dividir(valor1, valor2) {
    resultado = valor1 / valor2;
    return "El resultado de la division es " + resultado;
}
console.log(dividir(10,5))

//Programa que detecte que es Par o impar 

function parImpar(numero){
    numero = prompt("Ingresa un numero:");
    residuo = numero % 2;

    if(residuo == 1){
        alert("Es impar");
    }else{
        alert("Es par");
    }
}

console.log(parImpar());